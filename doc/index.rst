GNU Health patient (fiuner) module
################################

This module add support to:

     * Add "edad" function field, to display and search age in a different way on patient registers
     * Add "dob" field, to display and search on patient registers
     * Add "dob" function field, to display and search neighborhood on patient registers
     * Add "hc" (clinic history id) and "cronico" field to patient registers
