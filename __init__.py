from trytond.pool import Pool
from .health import *

def register():
    Pool.register(
        PatientData,
        module='health_patient_fiuner', type_='model')
